package ru.home.zaets.oauth.oauth;

import com.mastercard.developer.oauth.Util;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.PrivateKey;
import java.util.List;
import java.util.TreeMap;

import static ru.home.zaets.oauth.oauth.OAuth.*;
import static ru.home.zaets.oauth.oauth.OauthApplication.Test.key;

/**
 *
 * Максим,
 *
 * скорее всего неверно формируется SignatureBase строка. Пробовали на вашем мерчанте с вашим сертификатом - запрос проходит.
 *
 * Пример успешного запроса:
 *
 * _signatureBaseString = "POST&https%3A%2F%2Ftestwallet.masterpass.ru%2Fmpapiv2%2FGetRequestToken&oauth_body_hash%3DJpGRb8cM2ccAAPrSEFz4uHHVMQg%253D%26oauth_consumer_key%3Db0aa4a86df594edf8b87846d866bfbc2%26oauth_nonce%3D6b416d3b-2a9e-4a7b-bcc2-dcc315e22524%26oauth_signature_method%3DRSA-SHA1%26oauth_timestamp%3D1528897285%26oauth_version%3D1.0"
 *
 * Headers
 * Authorization: OAuth oauth_body_hash="JpGRb8cM2ccAAPrSEFz4uHHVMQg%3d",oauth_consumer_key="b0aa4a86df594edf8b87846d866bfbc2",oauth_nonce="6b416d3b-2a9e-4a7b-bcc2-dcc315e22524",oauth_signature="kENLUBLKQEXjyEehtWKB6BPUqNMb6wvTjQFN%2b8Qk6VwOKqmqsfDa3AnCxsOLA8B9Q9Hoa2s5CK51ZC5sEIhJINe7Ugnx58g7z%2flrGvnjnI73Dx00rur071t9qmW3sNMzOJJPJBDjqcPX74%2b53owpYRgSgiiWgL%2fC4JSyf7Ywz3fuFL%2b1fLZY4drE8k97oH%2bRtzHPFJQ37aXPtpWJ%2bZtv30G0dNgC7lTXS1x%2f8Dz0Jnpk6W41lE%2fI3WRRrfcr6lNg62NBqs9I38IjO%2fSBSXzoVPV%2flbtV63z6Y%2fUP9hi%2fbGjU034JqOJCkbUmzkKMeNf%2ffuaO19pJJZR8O5%2ft8Z7mUw%3d%3d",oauth_signature_method="RSA-SHA1",oauth_timestamp="1528897285",oauth_version="1.0"
 *
 * Body
 * MerchantName=Merchant&Phone=79160234234
 *
 * Url
 * https://testwallet.masterpass.ru
 * Method
 * mpapiv2/GetRequestToken
 *
 *
 * Created by dzaets on 22.06.2018.
 * oauth
 */
public class UberTest {
    static final String EMPTY_STRING = "";
    private static final String SHA_BITS = "1";

    public static void main(String[] args) throws MalformedURLException, URISyntaxException {
        String password = "f6c62ae5a8";
        String consumerKey = "b0aa4a86df594edf8b87846d866bfbc2";
        PrivateKey signingKey = key(password);
        Charset charset = Charset.forName("UTF-8");

        URL url = new URL("https://testwallet.masterpass.ru/mpapiv2/GetRequestToken");
        String payload = "MerchantName=Merchant&Phone=79160234234";

        String post = getAuthorizationHeader(url.toURI(), "POST", payload, charset, consumerKey, signingKey);


        System.out.println();
        String original = "OAuth oauth_body_hash=\"JpGRb8cM2ccAAPrSEFz4uHHVMQg%3d\",oauth_consumer_key=\"b0aa4a86df594edf8b87846d866bfbc2\",oauth_nonce=\"6b416d3b-2a9e-4a7b-bcc2-dcc315e22524\",oauth_signature=\"kENLUBLKQEXjyEehtWKB6BPUqNMb6wvTjQFN%2b8Qk6VwOKqmqsfDa3AnCxsOLA8B9Q9Hoa2s5CK51ZC5sEIhJINe7Ugnx58g7z%2flrGvnjnI73Dx00rur071t9qmW3sNMzOJJPJBDjqcPX74%2b53owpYRgSgiiWgL%2fC4JSyf7Ywz3fuFL%2b1fLZY4drE8k97oH%2bRtzHPFJQ37aXPtpWJ%2bZtv30G0dNgC7lTXS1x%2f8Dz0Jnpk6W41lE%2fI3WRRrfcr6lNg62NBqs9I38IjO%2fSBSXzoVPV%2flbtV63z6Y%2fUP9hi%2fbGjU034JqOJCkbUmzkKMeNf%2ffuaO19pJJZR8O5%2ft8Z7mUw%3d%3d\",oauth_signature_method=\"RSA-SHA1\",oauth_timestamp=\"1528897285\",oauth_version=\"1.0\"";
        if (post.equals(original)) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }

        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println();
        System.out.println(post);
    }


    public static String getAuthorizationHeader(URI uri, String method, String payload, Charset charset, String consumerKey, PrivateKey signingKey) {
        TreeMap<String, List<String>> queryParams = extractQueryParams(uri, charset);
        TreeMap<String, String> oauthParams = new TreeMap<>();
        oauthParams.put("oauth_consumer_key", consumerKey);
        oauthParams.put("oauth_nonce", "6b416d3b-2a9e-4a7b-bcc2-dcc315e22524");
        oauthParams.put("oauth_signature_method", "RSA-SHA" + SHA_BITS);
        oauthParams.put("oauth_timestamp", "1528897285");
        oauthParams.put("oauth_version", "1.0");
        if (payload != null && !payload.isEmpty()) {
            oauthParams.put("oauth_body_hash", getBodyHash(payload, charset));
        }

        // Combine query and oauth_ parameters into lexicographically sorted string
        String paramString = toOauthParamString(queryParams, oauthParams);

        // Normalized URI without query params and fragment
        String baseUri = getBaseUriString(uri);

        // Signature base string
        String sbs = getSignatureBaseString(method, baseUri, paramString, charset);

        //todo: чтобы было как у них
        sbs = sbs.replace("oauth_body_hash%3DJpGRb8cM2ccAAPrSEFz4uHHVMQg%3D%26", "oauth_body_hash%3DJpGRb8cM2ccAAPrSEFz4uHHVMQg%253D%26");

        System.out.println("getSignatureBaseString:             " + sbs);

        // Signature
        String signature = signSignatureBaseString(sbs, signingKey, charset);
        String encoded_signature = Util.percentEncode(signature, charset);

        encoded_signature = prepareSig(encoded_signature);

        System.out.println("signature:             " + encoded_signature);

        oauthParams.put("oauth_signature", encoded_signature);

        oauthParams.put("oauth_body_hash", oauthParams.get("oauth_body_hash").replace("=", "%3d"));

        return getAuthorizationString(oauthParams);
    }

}
