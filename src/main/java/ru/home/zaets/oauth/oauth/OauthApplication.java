package ru.home.zaets.oauth.oauth;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.net.ssl.HttpsURLConnection;
import java.io.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.Charset;
import java.security.*;
import java.security.cert.CertificateException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.Base64;
import java.util.stream.Collectors;

@SpringBootApplication
public class OauthApplication {

    public static void main(String[] args) {
        SpringApplication.run(OauthApplication.class, args);
    }

    @RestController("/")
    public static class Test {

        public static PrivateKey key(String password) {

            Key key = null;
            try (FileInputStream certFile = new FileInputStream(new File("D:\\elena\\git\\oauth-masterpass\\certificate.pfx"))) {
                KeyStore keyStore = KeyStore.getInstance("PKCS12");
                try {
                    keyStore.load(certFile, password.toCharArray());
                } catch (IOException | NoSuchAlgorithmException | CertificateException e) {
                    e.printStackTrace();
                }
                key = keyStore.getKey("1", password.toCharArray());
            } catch (IOException | UnrecoverableKeyException | NoSuchAlgorithmException | KeyStoreException e) {
                e.printStackTrace();
            }
            return (PrivateKey) key;
        }

        @GetMapping("get")
        public String get() {
            String password = "f6c62ae5a8";
            String consumerKey = "b0aa4a86df594edf8b87846d866bfbc2";
            PrivateKey signingKey = key(password);
            URI uri = URI.create("https://sandbox.api.mastercard.com/service");
            String method = "GET";
            String payload = "Hello world!";
            Charset charset = Charset.forName("UTF-8");

            String authHeader = OAuth.getAuthorizationHeader(uri, method, payload, charset, consumerKey, signingKey);
            return authHeader;
        }

        @GetMapping("get1")
        public String get1() throws IOException, URISyntaxException {
            String password = "f6c62ae5a8";
            String consumerKey = "b0aa4a86df594edf8b87846d866bfbc2";
            PrivateKey signingKey = key(password);

            URL url = new URL("https://testwallet.masterpass.ru/mpapiv2/GetRequestToken?MerchantName=Merchant&Phone=79160234234");

            return getToken(consumerKey, signingKey, url, "POST");
        }

        /**
         * https://developer.mastercard.com/support-article/how-can-i-validate-my-oauth-implementation
         *
         * @return
         * @throws Exception
         */
        @GetMapping("test")
        public String test() throws Exception {
            String pk = "-----BEGIN PRIVATE KEY-----\n" +
                    "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAKWHZ/XYTSdiQ6Kgy6kRJtMY3FZ2\n" +
                    "YMglyJATYDApUKIoXJrtW40lm4yq/zw1zHMEATdGqQz+6qliGu1qTBmiDwWDK8bXJMi28hkDBY3X\n" +
                    "EwY1bjV/8nbnMRzhGtwaIxrNKcrNYvhuWor3EBAQKFh72IXU7QU0TR/dPXQU1FR8R5etAgMBAAEC\n" +
                    "gYAlQ8CRxipIbYTQfeabnzLgXz5rcKcFKAxo8xO6g/KVLwT6E+mCcy77kht30JzAX+xuJ0gzlsf5\n" +
                    "bg6l2EJdmvBKbUvH/cwgEu02Z+4vC79EqgwTR3nmSz3pxF62mwAIi7OFGoBZQXqDYr1JAwGCFFBN\n" +
                    "555AvAosDv7S5YVehMliAQJBAPUxjzoRV47SDCS0eoMZxlOZbUoo0+R9srKhMDSmbiPRR4D8YwyN\n" +
                    "xg51WQib7X7kp7QlhQzf3NXFeBJO5A9f9MECQQCs0wQNlEth2QajCOChAfPlSeE9BR7px+gK3Lrk\n" +
                    "kttx7L9v+vDTdFUTWJxxPj/JYMt+ggUp1xpEqyNAmW1VjUHtAkAYZpg0VSl7gxfGR1ex2EzOYQRp\n" +
                    "TurXYFL6R+Q+ORnY4qjVA0jwJOPC6Ja1rp7R8/tkiB9Xiqe1dnNejw9PIGOBAkB6QkUJvjCdpcQW\n" +
                    "Lb+K5zC8sckPO8Ikq/CxTnlAHcv0CgFbnHAlhpRwvSzex6SkNz993Uj90leY4GBt4JwB+435AkEA\n" +
                    "qhzY/AlxIFUpG0zGpMfd+LUFKZ+wmazB6oFpAf/mbTN/EXF6kf40tCvpRWSnlETN6yJX7s26G+mX\n" +
                    "F0aeBlqNCA==\n" +
                    "-----END PRIVATE KEY-----";


            PrivateKey privateKey = privateKeyFromString(pk);

            String consumerKey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

            URL url = new URL("https://sandbox.api.mastercard.com/atms/v1/atm?Format=XML&PageOffset=0&PageLength=10&AddressLine1=70%20Main%20St&PostalCode=63366&Country=USA");

            return getToken(consumerKey, privateKey, url, "GET");
        }

        private String getToken(String consumerKey, PrivateKey signingKey, URL url, String method) throws IOException, URISyntaxException {
            Charset charset = Charset.forName("UTF-8");
            String payload = url.getQuery();

            HttpsURLConnection con = (HttpsURLConnection) url.openConnection();
            con.setRequestMethod(method);
//            con.setRequestProperty("Content-Type", "application/json; charset=" + charset.name());

            String authHeader = OAuth.getAuthorizationHeader(url.toURI(), method, payload, charset, consumerKey, signingKey);
            System.out.println("authHeader:              " + authHeader);

            con.setRequestProperty(OAuth.AUTHORIZATION_HEADER_NAME, authHeader);

            System.out.println();
            System.out.println("HTTP connection:       " + con);

            BufferedReader br;
            if (200 <= con.getResponseCode() && con.getResponseCode() <= 299) {
                br = new BufferedReader(new InputStreamReader(con.getInputStream()));
            } else {
                br = new BufferedReader(new InputStreamReader(con.getErrorStream()));
            }
            return br.lines().collect(Collectors.joining("    "));
        }

        public PrivateKey privateKeyFromString(String key) throws Exception {
            // Read in the key into a String
            StringBuilder pkcs8Lines = new StringBuilder();
            BufferedReader rdr = new BufferedReader(new StringReader(key));
            String line;
            while ((line = rdr.readLine()) != null) {
                pkcs8Lines.append(line);
            }

            // Remove the "BEGIN" and "END" lines, as well as any whitespace

            String privateKeyContent = pkcs8Lines.toString();
            privateKeyContent = privateKeyContent.replaceAll("\\n", "").replace("-----BEGIN PRIVATE KEY-----", "").replace("-----END PRIVATE KEY-----", "");

            // extract the private key

            KeyFactory kf = KeyFactory.getInstance("RSA");
            PKCS8EncodedKeySpec keySpecPKCS8 = new PKCS8EncodedKeySpec(Base64.getDecoder().decode(privateKeyContent));
            PrivateKey privKey = kf.generatePrivate(keySpecPKCS8);
            return privKey;
        }
    }

}
